//
//  ViewController.swift
//  v2t
//
//  Created by Ashish Jha on 1/27/18.
//  Copyright © 2018 Ashish Jha. All rights reserved.
//

import UIKit
import Speech

var langDict: [String: String] = ["en": "en-US", "hi": "hi-IN", "ar": "ar-SA", "cn": "zh-CN", "nl": "nl-NL", "fr": "fr-FR", "de": "de-DE", "it": "it-IT", "es": "es-ES", "ja": "ja-JP", "ko": "ko-KR", "pt": "pt-PT", "ru": "ru-RU", "th": "th-TH", "tr": "tr-TR"]
var languages = [String](langDict.keys)

public class ViewController: UIViewController, SFSpeechRecognizerDelegate, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate{
    
    // MARK: UIViewController
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        // Disable the record buttons until authorization has been granted.
        recordButton.isEnabled = false
        imageTextField.delegate = self
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        
        languageField.inputView = pickerView
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return languages.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return languages[row]
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        languageField.text = languages[row]
        self.speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: langDict[languageField.text!]!))!
        self.view.endEditing(true)
    }
    
    
    // MARK: Properties
    
    private var speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))!
    
    private var recognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    
    private var recognitionTask: SFSpeechRecognitionTask?
    
    private let audioEngine = AVAudioEngine()
    
    @IBOutlet var imageTextField : UITextField!
    
    @IBOutlet var textView : UITextView!
    
    @IBOutlet var recordButton : UIButton!
    
    @IBOutlet weak var generatedImage: UIImageView!
    
    @IBOutlet weak var languageField : UITextField!

    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.view.endEditing(true)
        self.imageTextField.resignFirstResponder()
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        self.generateImage(inputText: textField.text!)
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        speechRecognizer.delegate = self
        
        SFSpeechRecognizer.requestAuthorization { authStatus in
            /*
             The callback may not be called on the main thread. Add an
             operation to the main queue to update the record button's state.
             */
            OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.recordButton.isEnabled = true
                    
                case .denied:
                    self.recordButton.isEnabled = false
                    self.recordButton.setTitle("User denied access to speech recognition", for: .disabled)
                    
                case .restricted:
                    self.recordButton.isEnabled = false
                    self.recordButton.setTitle("Speech recognition restricted on this device", for: .disabled)
                    
                case .notDetermined:
                    self.recordButton.isEnabled = false
                    self.recordButton.setTitle("Speech recognition not yet authorized", for: .disabled)
                }
            }
        }
    }
    
    func addPercentageToUrl(urlString : String) -> String{
        
        return urlString.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
    }
    
    func translateText(inputText : String) -> String {
        let session = URLSession.shared
        var url_string = "https://translate.googleapis.com/translate_a/single?client=gtx&sl=" + self.languageField.text! + "&tl=en&dt=t&q=" + inputText
        url_string = addPercentageToUrl(urlString: url_string)
        print(url_string)
        let url = URL(string: url_string)!
        var returnText = "nothing"
        DispatchQueue.global(qos: .userInitiated).async {
            let task = session.dataTask(with: url) { (data, _, _) -> Void in
                if let data = data {
                    //                let string = String(data: data, encoding: String.Encoding.utf8)
                    //                print(string) //JSONSerialization
                    DispatchQueue.main.async {
                        let translateRespString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                        //print(translateRespString)
                        do {
                            let array = try JSONSerialization.jsonObject(with: data) as? [[String : Any]]
                            //print(array)
                        } catch {
                            print("Exception occured \(error))")
                        }
                    }
                }
            }
            task.resume()
        }
        return inputText
    }
    
    func generateImage(inputText : String) -> Bool {
        let inputText2 = translateText(inputText: inputText)
        let session = URLSession.shared
        var url_string = "http://ec2-54-164-195-92.compute-1.amazonaws.com:8080/api-0.0.1/test/texttoimageurl?q=" + inputText2
        url_string = addPercentageToUrl(urlString: url_string)
        print(url_string)
        let url = URL(string: url_string)!
        DispatchQueue.global(qos: .userInitiated).async {
            let task = session.dataTask(with: url) { (data, _, _) -> Void in
                if let data = data {
                    //                let string = String(data: data, encoding: String.Encoding.utf8)
                    //                print(string) //JSONSerialization
                    DispatchQueue.main.async {
                        let image = UIImage(data : data)
                        self.generatedImage.image = image
                        // self.generatedImage.sizeToFit()
                    }
                }
            }
            task.resume()
        }
        return true
    }
    
    private func startRecording() throws {
        
        // Cancel the previous task if it's running.
        if let recognitionTask = recognitionTask {
            recognitionTask.cancel()
            self.recognitionTask = nil
        }
        
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSessionCategoryRecord)
        try audioSession.setMode(AVAudioSessionModeMeasurement)
        try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        
        recognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = audioEngine.inputNode
        guard let recognitionRequest = recognitionRequest else { fatalError("Unable to created a SFSpeechAudioBufferRecognitionRequest object") }
        
        // Configure request so that results are returned before audio recording is finished
        recognitionRequest.shouldReportPartialResults = true
        
        // A recognition task represents a speech recognition session.
        // We keep a reference to the task so that it can be cancelled.
        recognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) { result, error in
            var isFinal = false
            
            if let result = result {
                self.textView.text = result.bestTranscription.formattedString
                isFinal = result.isFinal
            }
            
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.generateImage(inputText: self.textView.text!)
                
                self.recognitionRequest = nil
                self.recognitionTask = nil
                
                self.recordButton.isEnabled = true
                self.recordButton.setTitle("Start Recording", for: [])
            }
        }
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.recognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        
        try audioEngine.start()
        
        textView.text = "(Go ahead, I'm listening)"
    }
    
    // MARK: SFSpeechRecognizerDelegate
    
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            recordButton.isEnabled = true
            recordButton.setTitle("Start Recording", for: [])
        } else {
            recordButton.isEnabled = false
            recordButton.setTitle("Recognition not available", for: .disabled)
        }
    }
    
    // MARK: Interface Builder actions
    
    @IBAction func recordButtonTapped() {
        if audioEngine.isRunning {
            audioEngine.stop()
            recognitionRequest?.endAudio()
            recordButton.isEnabled = false
            recordButton.setTitle("Stopping", for: .disabled)
        } else {
            try! startRecording()
            recordButton.setTitle("Stop recording", for: [])
        }
    }
    
}
